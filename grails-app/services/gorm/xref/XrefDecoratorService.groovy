package gorm.xref

/**
 * Enhances domain classes that end with 'Xref' and adds dynamic methods to each
 * respective class defined in a xref static field.
 * 
 * e.g.
 * class AuthorBookXref {
 *     Author author
 *     Book book
 *     static xref = [authors: Book, books: Author]
 * }
 *
 * Notice the property names are swapped from a typical hasMany definition. The
 * key value is used as the dynamic field of the referenced class that follows.
 * This allows access of the form
 *
 * bookInstance.authors
 * bookInstance.addToAuthors(authorInstance)
 * bookInstance.removeFromAuthors(authorInstance)
 * 
 */
class XrefDecoratorService {

    static transactional = false

    def enhanceXrefDomains(domainClasses) {
        def xrefClasses = domainClasses.findAll { it.name.endsWith('Xref') } 
        for (dc in xrefClasses) {
            XrefEnhancer.enhanceXrefClass(dc)
        }
    }

    def enhanceXrefClass(dc) {
        if (dc.hasMetaProperty('xref')) {
            // very awkward, I know
            def xref = dc.clazz.xref.entrySet().toList()

            decorateClass(dc, xref[0], xref[1])
            decorateClass(dc, xref[1], xref[0])
        }
    }

    private decorateClass(dc, left, right) {
        def xrefPropName = left.key
        def refClass = left.value
        
        def metaClass = refClass.metaClass
        def xrefName = xrefPropName.capitalize()
        def refPropName = StringUtils.uncapitalize(refClass.simpleName)

        metaClass."addTo${xrefName}" = { obj, flush = false  ->
            def objPropName = StringUtils.uncapitalize(obj.class.simpleName)
            def instance = dc.clazz.newInstance()
            instance."${objPropName}" = obj
            instance."${refPropName}" = delegate
            instance.save(flush: flush, insert: true)
        }

        metaClass."get${xrefName}" = { ->
            def otherSideProp = StringUtils.uncapitalize(right.value.simpleName)
            dc.clazz."findAllBy${refClass.simpleName}"(delegate).collect { it."${otherSideProp}" } as Set
        }

       metaClass."removeFrom${xrefName}" = { obj, flush = false ->
            def instance = dc.clazz."findBy${refClass.simpleName}And${obj.class.simpleName}"(delegate, obj)
            if (instance) {
                instance.delete(flush: flush)
            }
        }

        if (!metaClass.hasMetaMethod('beforeDelete')) {
            metaClass."beforeDelete" = { ->
                def d = delegate
                refClass.withNewSession { 
                    dc.clazz."findAllBy${refClass.simpleName}"(d)*.delete(flush: true)
                }
            }
        }
    }
}
