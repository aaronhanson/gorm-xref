#GORM Xref Grails Plugin
In order to support high performance many to many relationships, it's often necessary to create a join class that can be inserted into without the penalty of the default hasMany relationship. This plugin uses convention to decorate classes involved in a join class allowing for convenient access methods as if a hasMany relationship had been defined.

```
class Book {
    String title
}

class Author {
    String name
}

class AuthorBookXref implements Serializable {
    Author author
    Book book

    static xref = [authors: Book, books: Author]

    static mapping = {
        id composite: ['author', 'book']
        version false
    }
}
```

```
bookInstance.authors
bookInstance.addToAuthors(authorInstance)
bookInstance.removeFromAuthors(authorInstance)
```

#Note
This is only an Alpha version of the plugin. Much still needs to be done. Code cleanup, defensive stuff, and configuration options to allow for various naming conventions in case you would prefer Assoc or Relation etc instead of Xref. Also, I'm pondering adding something into the referenced classes possibly. Basically, use at your own risk and don't be surprised if things change significantly for a bit.
