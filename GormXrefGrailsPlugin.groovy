import gorm.xref.XrefDecoratorService
import groovy.util.logging.Commons

@Commons
class GormXrefGrailsPlugin {
    def version = "0.1"
    def grailsVersion = "2.2 > *"
    def pluginExcludes = [
        "grails-app/views/error.gsp"
    ]

    def title = "Gorm Xref Plugin"

    def description = '''\
Adds dynamic methods for many-to-many relations.'''

    def documentation = "https://bitbucket.org/aaronhanson/gorm-xref"

    def license = "APACHE"

    def developers = [
        [ name: "Aaron Hanson" ]
    ]

    def issueManagement = [ system: "bitbucket", url: "https://bitbucket.org/aaronhanson/gorm-xref/issues" ]

    def doWithDynamicMethods = { ctx ->
        decorateDomainClasses(application.domainClasses)
    }

    def onChange = { event ->
        decorateDomainClasses(application.domainClasses)
    }

    private decorateDomainClasses(domainClasses) {
        def xrefDecoratorService = new XrefDecoratorService()
        
        log.debug("Decorating domain classes with Many-To-Many methods")
        xrefDecoratorService.enhanceXrefDomains(application.domainClasses)
        log.debug("Done decorating domain classes with Many-To-Many methods")
    }
}
